<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('test',function(){
     $users=\App\User::all();
    return response($users,200);
});
Route::prefix("v1")->middleware('auth:api')->group(function(){
    Route::get('user-list','UserController@getUsers')->name('user-list');
    Route::post('get-user-conversation','ChatController@getUserConversationById')->name('get-user-conversation');
    Route::post('chat-save','ChatController@sendNewMessage')->name('chat-save');
});
