<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $users=[
            'name'=>'alireza',
            'email'=>'ali@gmail.com',
            'password'=>bcrypt(123)
        ];
        \App\User::create($users);
    }
}
