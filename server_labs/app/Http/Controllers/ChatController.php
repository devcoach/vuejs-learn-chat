<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;
class ChatController extends Controller
{
    public function getUserConversationById(Request $request){
        $userId=$request->input('user_id');
        $authId = $request->user()->id;
        
        $chats = Chat::whereIn('sender_id',[
            $authId,
            $userId
            ])->whereIn('receiver_id',[$authId,$userId])
        ->orderBy('created_at','asc')
        ->get();
        return response()->json($chats);
    }
    public function sendNewMessage(Request $request)
    {
        $message= $request->input('chat');
        $receiver_id=$request->input('receiver_id');
        $chat = Chat::create([
            'sender_id'=>$request->user()->id,
            'receiver_id'=>$receiver_id,
            'chat'=>$message,
            'read'=>1
        ]);
        return response()->json($chat,201);
    }
}
