const state={
    userAuth:null
}

const mutations={
    SET_USER_AUTH(state,userObject){
        state.userAuth=userObject
    },
    CLEAR_USER_AUTH(state){
        state.userAuth=null
    }
}

const actions={
    setUser:({commit},userObject)=>{
        commit("SET_USER_AUTH",userObject);
    },
    clearUser:({commit})=>{
        commit('CLEAR_USER_AUTH');
    }
}

export default{
    state,mutations,actions
  
}