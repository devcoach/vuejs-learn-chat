import Vue from 'vue';
import {userListUrl,getHeaders,setChatUserUrl,chatSaveUrl} from '../../configs';
const state={
    userList:[],
    currentChatUser:null,
    conversation:null
}
const mutations={
    SET_USER_LIST(state,userList){
        state.userList=userList;
    },
    SET_CURRENT_CHAT_USER(state,user){
        state.currentChatUser=user;
    },
    SET_CONVERSATION(state,conversation){
        state.conversation=conversation;
    },
    SET_CHAT_NEW_MESSAGE(state,message){
        state.conversation.push(message);
    }
}
const actions={

    setUserList:({commit},userList)=>{
        Vue.http.get(userListUrl,{headers:getHeaders()}).then(response=>{
            
            if(response.status===200){
                commit('SET_USER_LIST',response.data);
            }
        });
    },
    setCurrentChatUser:({commit},user)=>{
        //commit('SET_CURRENT_CHAT_USER',user);
        let postData={user_id:user.id};
        Vue.http.post(setChatUserUrl,postData,{headers:getHeaders()}).then(response=>{
            if(response.status===200){
                commit('SET_CURRENT_CHAT_USER',user);
                commit('SET_CONVERSATION',response.data);
            }
        });
    },
    addNewMessage:({commit},postData)=>{
        Vue.http.post(chatSaveUrl,postData,{headers:getHeaders()}).then(res=>{
           commit('SET_CHAT_NEW_MESSAGE',res.data);     
        });
    }
}
export default{
    state,mutations,actions
}