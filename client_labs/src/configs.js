const apiUrl='http://127.0.0.1:8000/';
const loginUrl=apiUrl+'oauth/token';
const userUrl= apiUrl+'/api/user';
const userListUrl=apiUrl+'api/v1/user-list';
const setChatUserUrl=apiUrl+'api/v1/get-user-conversation';
const chatSaveUrl=apiUrl+'api/v1/chat-save';
const getHeaders =()=>{
     const tokenData= JSON.parse(window.localStorage['userAuth']);
    
     const headers = {
         'Content-Type':"application/json",
         'Authorization':"Bearer "+ tokenData.access_token
     }
     return headers;
}
export{
    apiUrl,
    loginUrl,
    userUrl,
    getHeaders,
    userListUrl,
    setChatUserUrl,
    chatSave,
    chatSaveUrl
}