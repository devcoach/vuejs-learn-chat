import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
import userStore  from './components/user/userStore'
import chatStore  from './components/chat/chatStore'
const debug = process.env.NODE_ENV !=='production';
export default new Vuex.Store({
    modules:{
        userStore,
        chatStore
    },

    strict:debug
});