// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import vueResource from 'vue-resource';
import vueRouter from 'vue-router';
import store from './Store';
import Dashboard from './components/pages/Dashboard'
import LoginPage from './components/pages/LoginPage';
import ChatPage from './components/pages/ChatPage';
Vue.use(vueRouter);
Vue.use(vueResource);
Vue.component('app',App);

Vue.config.productionTip = false
const routes = [
  {name:'home',path:'/',component:LoginPage},
  {name:'dashboard',path:'/dashboard',component:Dashboard,meta: { requiresAuth: true }},
  {name:'chat',path:'/chat',component:ChatPage,meta: { requiresAuth: true }}
]
const router= new vueRouter({
  mode:'history',
  routes
});

router.beforeEach((to, from, next) => {
  // ...
  
 if(to.meta.requiresAuth){
  const userAuth = JSON.parse(window.localStorage.getItem('userAuth'));
  if(userAuth && userAuth.access_token){
    next();
  }else{
    next({name:"home"})
  }
 }
 next();
})

/* eslint-disable no-new */

new Vue({
  router,store
}).$mount('#app');
